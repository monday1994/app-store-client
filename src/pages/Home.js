import React, { Component } from 'react';

import Navbar from '../components/Navbar';

import '../css/stylesheet.css';
import Header from "../components/Header";

class Home extends Component {



    render() {
        return (
            <div className="page">
                <Header />
                <Navbar role="home"/>
            </div>
        );
    }
}

export default Home;
