import React, { Component } from 'react';

import Navbar from '../components/Navbar';
import UserPageDisplayStore from '../stores/UserStores/UserPageDisplayStore';


import GamesMainComponent from "../components/UserComponents/Games/GamesMainComponent";
import GamesStore from '../stores/AdminStores/GamesStore';

import '../css/stylesheet.css';
import Header from "../components/Header";
class User extends Component {
    componentWillMount(){
        UserPageDisplayStore.addListener('change', this.handleDisplayChange);

        this.setState({
            componentsVisibility : UserPageDisplayStore.getComponentsVisibility()
        });
    }

    componentDidMount(){
        GamesStore.initializeStore();
    }

    componentWillUnmount(){

        UserPageDisplayStore.removeListener('change', this.handleDisplayChange);
    }
    constructor(props){
        super(props);

        this.state = {
            componentsVisibility : {}
        };
        this.handleDisplayChange = this.handleDisplayChange.bind(this);
    }

    handleDisplayChange(){
        this.setState({
            componentsVisibility : UserPageDisplayStore.getComponentsVisibility()
        });
    }



    render() {
        return (
            <div className="page">
                <Header/>
                <Navbar role="user"/>

                <div>
                    {this.state.componentsVisibility['gamesMainComponent'] ? <GamesMainComponent /> : null}
                </div>
            </div>
        );
    }
}

export default User;