import React, {Component} from 'react';
import MozillaFormLib from 'react-jsonschema-form';
import formSchema from '../constants/formsSchemas';
import PropTypes from 'prop-types';
import * as authCalls from '../modules/api/authCalls';

import Navbar from '../components/Navbar';

import '../css/stylesheet.css';
import Header from "../components/Header";
import webStorageWrapper from '../modules/webStorageWrapper';

const Form = MozillaFormLib;

class Login extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    componentWillMount(){
        this.setState({
            schema : formSchema.REGISTRATION_FORM_SCHEMA
        });
    }

    constructor(props){
        super(props);

        this.state = {
            schema : undefined
        };

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit({formData}){
        authCalls.registrationCall(formData).then(user => {
            webStorageWrapper.add('user', JSON.stringify(user));

            this.context.router.history.push('/user');
        }).catch(err => {
            console.log("err in auth, err = ", err);
            alert('something went wrong');
        })
    }

    render(){
        console.log("schema = ", this.state);
        return (
            <div className="page">
                <Header />
                <Navbar role="home"/>
                <div className="form">
                    <Form
                        schema={this.state.schema}
                        uiSchema={undefined}
                        onSubmit={this.onSubmit}
                    />
                </div>
            </div>
        );
    }
}

export default Login;

