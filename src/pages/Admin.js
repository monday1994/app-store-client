import React, { Component } from 'react';

import Navbar from '../components/Navbar';
import UserPageDisplayStore from '../stores/UserStores/UserPageDisplayStore';
import GamesStore from '../stores/AdminStores/GamesStore';
import PropTypes from 'prop-types';

import '../css/stylesheet.css';
import GamesMainComponent from "../components/AdminComponents/Games/GamesMainComponent";
import Header from "../components/Header";

import webStorageWrapper from '../modules/webStorageWrapper';

class Admin extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    componentWillMount(){

        let user = JSON.parse(webStorageWrapper.get('user'));

        if(user){
            if(user.role !== 'admin'){
                alert('You are not admin, please log in to see admin page');
                this.context.router.history.push('/login');
            }
        } else {
            alert('You are not admin, please log in to see admin page');
            this.context.router.history.push('/login');
        }



        UserPageDisplayStore.addListener('change', this.handleDisplayChange);

        this.setState({
            componentsVisibility : UserPageDisplayStore.getComponentsVisibility()
        });
    }

    componentDidMount(){
        //stores initialize
        GamesStore.initializeStore();
    }

    componentWillUnmount(){

        UserPageDisplayStore.removeListener('change', this.handleDisplayChange);
    }
    constructor(props){
        super(props);

        this.state = {
            componentsVisibility : {}
        };
        this.handleDisplayChange = this.handleDisplayChange.bind(this);
    }

    handleDisplayChange(){
        this.setState({
            componentsVisibility : UserPageDisplayStore.getComponentsVisibility()
        });
    }



    render() {
        return (
            <div className="page">
                <Header/>
                <Navbar role="admin"/>

                <div>
                    {this.state.componentsVisibility['gamesMainComponent'] ? <GamesMainComponent /> : null}
                </div>
            </div>
        );
    }
}

export default Admin;