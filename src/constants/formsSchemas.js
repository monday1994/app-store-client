const createGameFormSchema = {
    title: "Upload Game",
    type: "object",
    required: ["name", "description","thumbnail", "game"],
    properties: {
        name: {type: "string", title: "Name", default: "Game name"},
        description: {type: "string", title: "Description"},
        version : {type : "string", title : "Version", default : "0.0.1"},
        thumbnail: {type : "string", title : "Game thumbnail"},
        game: {type : "string", title: "Game exe"}
    }
};

const createGameFormUiSchema = {
    thumbnail : {
        "ui:widget": "file"
    },
    game : {
        "ui:widget": "file"
    }
};

const editGameFormSchema = {
    title: "Edit Game",
    type: "object",
    required: ["name", "description","thumbnail", "game"],
    properties: {
        id : {type : "number", title : "ID" },
        name: {type: "string", title: "Name"},
        description: {type: "string", title: "Description"},
        version : {type : "string", title : "Version"},
        thumbnail: {type : "string", title : "Game thumbnail"},
        game: {type : "string", title: "Game exe"}
    }
};

const editGameFormUiSchema = {
    id : {"ui:readonly" : true},
    thumbnail : {
        "ui:widget": "file"
    },
    game : {
        "ui:widget": "file"
    }
};

const loginFormSchema = {
    title: "Login",
    type: "object",
    required: ["email", "password"],
    properties: {
        email : {type : "string", title : "Email" },
        password: {type: "string", title: "Password"}
    }
};

const registrationFormSchema = {
    title: "Registration",
    type: "object",
    required: ["email", "password"],
    properties: {
        email : {type : "string", title : "Email" },
        password: {type: "string", title: "Password"}
    }
};

let schemas = {
    CREATE_GAME_FORM_SCHEMA : createGameFormSchema,
    CREATE_GAME_FORM_UI_SCHEMA : createGameFormUiSchema,
    EDIT_GAME_FORM_SCHEMA : editGameFormSchema,
    EDIT_GAME_FORM_UI_SCHEMA : editGameFormUiSchema,
    LOGIN_FORM_SCHEMA : loginFormSchema,
    REGISTRATION_FORM_SCHEMA : registrationFormSchema
};

export default schemas;
