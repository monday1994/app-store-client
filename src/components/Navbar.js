import React, {Component} from 'react';
import * as UserPageDisplayActions from '../actions/UserActions/UserPageDisplayActions';
import * as AdminPageDisplayActions from '../actions/AdminActions/AdminPageDisplayActions';
import Nav from './Navbar/Nav';
import NavItem from './Navbar/NavItem';
import NavRouteItem from "./Navbar/NavRouteItem";
import PropTypes from 'prop-types';
import webstorageWrapper from '../modules/webStorageWrapper';

class Navbar extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    constructor(props){
        super(props);

        this.state = {};

        this.handleUserNavChange = this.handleUserNavChange.bind(this);
        this.handleAdminNavChange = this.handleAdminNavChange.bind(this);
    }

    handleUserNavChange(e, componentName){
        switch(componentName){
            case 'gamesList' :
                UserPageDisplayActions.setComponentsVisibility(['gamesMainComponent', 'gamesList']);
                break;
            case 'logout' :
                webstorageWrapper.clear();
                this.context.router.history.push('/');
                break;
        }
    }

    handleAdminNavChange(e, componentName){
        e.preventDefault();
        console.log("componnet name = ", componentName);
        switch(componentName){
            case 'gamesList' :
                AdminPageDisplayActions.setComponentsVisibility(['gamesMainComponent', 'gamesList']);
                break;
            case 'createGame' :
                AdminPageDisplayActions.setComponentsVisibility(['gamesMainComponent', 'createGame']);
                break;
            case 'logout' :
                webstorageWrapper.clear();
                this.context.router.history.push('/');
                break;
        }
    }

    render(){

        let navbar = null;

        switch(this.props.role){
            case 'home' :
                navbar = <Nav>
                    <NavRouteItem name="Home" href="/"/>
                    <NavRouteItem name="Login" href="/login"/>
                    <NavRouteItem name="Register" href="/registration"/>
                </Nav>;
                break;
            case 'user' :
                navbar = <Nav>
                    <NavItem name="Games" itemName='gamesList' onChangeComponentVisibility={this.handleUserNavChange}/>
                    <NavItem name="Log out" itemName='logout' onChangeComponentVisibility={this.handleAdminNavChange}/>
                </Nav>;
                break;
            case 'admin' :
                navbar = <Nav>
                    <NavItem name="Games" itemName='gamesList' onChangeComponentVisibility={this.handleAdminNavChange}/>
                    <NavItem name="Create Game" itemName='createGame' onChangeComponentVisibility={this.handleAdminNavChange}/>
                    <NavItem name="Log out" itemName='logout' onChangeComponentVisibility={this.handleAdminNavChange}/>
                </Nav>;
                break;
        }

        return (
            <div>
                {navbar}
            </div>

        );
    }
}

export default Navbar;

