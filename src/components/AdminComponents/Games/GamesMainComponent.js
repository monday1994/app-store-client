import React, {Component} from 'react';

import GamesList from "./GamesList";
import CreateGameForm from "./CreateGameForm";
import EditGameForm from "./EditGameForm";
import AdminPageDisplayStore from '../../../stores/AdminStores/AdminPageDisplayStore';

class GamesMainComponent extends Component {

    componentWillMount(){
        AdminPageDisplayStore.addListener('change', this.handleDisplayChange);

        this.setState({
            componentsVisibility : AdminPageDisplayStore.getComponentsVisibility()
        });
    }

    componentWillUnmount(){
        AdminPageDisplayStore.removeListener('change', this.handleDisplayChange);
    }

    constructor(props){
        super(props);

        this.state = {
            componentsVisibility : {}
        };

        this.handleDisplayChange = this.handleDisplayChange.bind(this);
    }

    handleDisplayChange(){
        this.setState({
            componentsVisibility : AdminPageDisplayStore.getComponentsVisibility()
        });
    }



    render(){
        return (
            <div>
                {this.state.componentsVisibility['gamesList'] ? <GamesList /> : null}
                {this.state.componentsVisibility['createGame'] ? <CreateGameForm/> : null}
                {this.state.componentsVisibility['editGame'] ? <EditGameForm/> : null}
            </div>
        );
    }
}

export default GamesMainComponent;
