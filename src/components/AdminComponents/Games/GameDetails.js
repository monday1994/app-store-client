import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import * as GamesActions from '../../../actions/AdminActions/GamesActions';
import * as AdminPageDisplayActions from '../../../actions/AdminActions/AdminPageDisplayActions';



import '../../../css/stylesheet.css';


const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};

class GameDetails extends Component {

    componentWillMount(){
    }

    componentDidMount(){
    }

    componentWillUnmount(){

    }
    constructor(props){
        super(props);
    }
    render(){

        return (
            <div >
                <Modal
                    isOpen={this.props.isModalVisible}
                    onRequestClose={this.props.showDetails}
                    style={customStyles}
                    contentLabel="Game Details"
                >
                <div className="game-elem-modal">
                    <img src={this.props.game.thumbnail}/>
                    <div className="game-info-modal">
                        <b>{this.props.game.name}</b>
                        <p>
                            {this.props.game.description}
                        </p>
                        <p>
                            version : {this.props.game.version}
                        </p>
                    </div>
                </div>
                </Modal>
            </div>
        );
    }
}

export default GameDetails;

