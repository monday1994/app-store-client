import React, {Component} from 'react';

import AdminPageDisplayStore from '../../../stores/AdminStores/AdminPageDisplayStore';
import GamesStore from '../../../stores/AdminStores/GamesStore';
import Game from './Game';

import webStorageWrapper from '../../../modules/webStorageWrapper';

class GamesList extends Component {

    componentWillMount(){
        AdminPageDisplayStore.addListener('change', this.handleDisplayChange);
        GamesStore.addListener('change', this.handleChange);
        this.setState({
            componentsVisibility : AdminPageDisplayStore.getComponentsVisibility()
        });
    }

    componentDidMount(){
        this.setState({
            games : GamesStore.getGames()
        });
    }

    componentWillUnmount(){
        AdminPageDisplayStore.removeListener('change', this.handleDisplayChange);
        GamesStore.removeListener('change', this.handleChange);
    }
    constructor(props){
        super(props);

        this.state = {
            componentsVisibility : {},
            games : []
        };
        this.handleDisplayChange = this.handleDisplayChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(){
        this.setState({
            games : GamesStore.getGames()
        });
    }

    handleDisplayChange(){
        this.setState({
            componentsVisibility : AdminPageDisplayStore.getComponentsVisibility()
        });
    }


    render(){

        let gamesList = null;
        if(this.state.games.length){
            gamesList = this.state.games.map((currentGame, i) => {
                console.log("current game in map = ", currentGame);
                return <Game key={i} downloadGame={this.download} game={currentGame}/>
            });
        }

        let style = {
            clear : 'both'
        };

        return (
            <div>
                {gamesList}
                <span style={style}></span>
            </div>
        );
    }
}

export default GamesList;

