import React, {Component} from 'react';
import GameDetails from './GameDetails';

import * as GamesActions from '../../../actions/AdminActions/GamesActions';
import * as AdminPageDisplayActions from '../../../actions/AdminActions/AdminPageDisplayActions';

import '../../../css/stylesheet.css';

class Game extends Component {

    componentWillMount(){
    }

    componentDidMount(){
    }

    componentWillUnmount(){

    }
    constructor(props){
        super(props);

        this.state = {
            showDetails : false
        };

        this.showDetails = this.showDetails.bind(this);
        this.edit = this.edit.bind(this);
        this.remove = this.remove.bind(this);
        this.assignGameToUser = this.assignGameToUser.bind(this);
    }

    showDetails(e){
        e.preventDefault();
        this.setState({
            showDetails : !this.state.showDetails
        });
    };

    edit(e, game){
        e.preventDefault();
        GamesActions.setGameToBeEdit(game);
        AdminPageDisplayActions.setComponentsVisibility(['gamesMainComponent', 'editGame']);
    }

    remove(e, game){
        e.preventDefault();
        GamesActions.removeGame(game);
    }

    assignGameToUser(e){
       // e.preventDefault();
        console.log("clicked");
        console.log("game can be assigned to user");
    }

    render(){

        let details = null;
        if(this.state.showDetails){
            details = <div className="game-details">
                <p>{this.props.game.description}</p>
                <p>version : {this.props.game.version}</p>
            </div>
        }

        return (
            <div className="game-elem">
                <img src={this.props.game.thumbnail} onClick={this.showDetails}/>
                <div className="game-info">
                    <b>{this.props.game.name}</b>
                </div>
                <div className="game-buttons">
                    <a href={this.props.game.game} onClick={(e) => this.assignGameToUser(e)} download>Download</a>
                    <button onClick={e => this.edit(e, this.props.game)}>edit</button>
                    <button onClick={e => this.remove(e, this.props.game)}>remove</button>
                </div>
                <p>{this.state.showDetails ? <GameDetails isModalVisible={this.state.showDetails} showDetails={this.showDetails} game={this.props.game}/> : null}</p>
            </div>
        );
    }
}

export default Game;

