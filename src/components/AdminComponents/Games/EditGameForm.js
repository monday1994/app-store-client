import React, {Component} from 'react';
import MozillaFormLib from 'react-jsonschema-form';
import {Button, ProgressBar, ControlLabel, FormControl} from 'react-bootstrap';
import AdminPageDisplayStore from '../../../stores/AdminStores/AdminPageDisplayStore';
import * as AdminPageDisplayActions from '../../../actions/AdminActions/AdminPageDisplayActions';

import GamesStore from '../../../stores/AdminStores/GamesStore';
import * as GamesActions from '../../../actions/AdminActions/GamesActions';


import ReactDOM from 'react-dom';
import formSchema from '../../../constants/formsSchemas';
import '../../../css/stylesheet.css';
const Form = MozillaFormLib;



class EditGameForm extends Component {

    componentWillMount(){

        AdminPageDisplayStore.addListener('change', this.handleDisplayChange);

        let rawGameToBeEdit = JSON.parse(JSON.stringify(GamesStore.getGameToBeEdit()));

        rawGameToBeEdit.thumbnail = ';base64,';
        rawGameToBeEdit.game = ';base64,';

        this.setState({
            schema : formSchema.EDIT_GAME_FORM_SCHEMA,
            uiSchema : formSchema.EDIT_GAME_FORM_UI_SCHEMA,
            gameToBeEdit : rawGameToBeEdit
        });
    }

    componentWillUnmount(){
        AdminPageDisplayStore.removeListener('change', this.handleDisplayChange);
    }

    constructor(props){
        super(props);

        this.state = {
            schema : undefined,
            uiSchema : undefined,
            progressValue : 0,
            gameToBeEdit : undefined
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleDisplayChange = this.handleDisplayChange.bind(this);
    }

    onSubmit({formData}){

        GamesActions.editGame(formData);
        AdminPageDisplayActions.setComponentsVisibility(['gamesMainComponent', 'gamesList']);

        /*let configName = ReactDOM.findDOMNode(this.refs.configName).value;
        console.log("configName = "+configName);
        if(validator.validateName(configName)){

            FormsActions.createConfig({config : formData, name : configName, schemaId : this.state.id});
            UserPageDisplayActions.setComponentsVisibility(['configsMainComponent', 'createConfig', 'progressBar']);
        } else {
            alert('invalid name');
        }*/
    }

    handleChange(){
        this.setState({
            progressValue: GamesStore.getProgressValue()
        });
    }

    handleDisplayChange(){
        this.setState({
            componentsVisibility : AdminPageDisplayStore.getComponentsVisibility()
        })
    }


    render(){
        const form = <div className="form">
            <Form
                schema={this.state.schema}
                uiSchema={this.state.uiSchema}
                formData={this.state.gameToBeEdit}
                onSubmit={this.onSubmit}
            />
        </div>;

        const progressBar = <div>
            <h2>Uploading</h2>
            <ProgressBar now={this.state.progressValue} />
        </div>;

        return (
            <div>
                {this.state.progressValue ? false : form}
                {this.state.progressValue ? progressBar : null}
            </div>
        );
    }
}

export default EditGameForm;
