import React, {Component} from 'react';

class Game extends Component {

    componentWillMount(){
    }

    componentDidMount(){
    }

    componentWillUnmount(){

    }
    constructor(props){
        super(props);

        this.state = {
            showDetails : false
        };

        this.showDetails = this.showDetails.bind(this);
    }

    showDetails(e){
        e.preventDefault();
        this.setState({
            showDetails : !this.state.showDetails
        });
    };

    render(){

        let details = null;
        if(this.state.showDetails){
            details = <div className="game-details">
                <p>{this.props.game.description}</p>
                <p>version : {this.props.game.version}</p>
            </div>
        }

        return (
            <div className="game-elem">
                <img src={this.props.game.thumbnail} onClick={this.showDetails}/>
                <div className="game-info">
                    <b>{this.props.game.name}</b>
                    <p>{this.state.showDetails ? details : null}</p>
                </div>
                <div className="game-buttons">
                    <a href={this.props.game.game} download>Download</a>
                </div>
            </div>
        );
    }
}

export default Game;

