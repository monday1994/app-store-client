import React, {Component} from 'react';

import GamesList from "./GamesList";
import UserPageDisplayStore from '../../../stores/UserStores/UserPageDisplayStore';

class GamesMainComponent extends Component {

    componentWillMount(){
        UserPageDisplayStore.addListener('change', this.handleDisplayChange);

        this.setState({
            componentsVisibility : UserPageDisplayStore.getComponentsVisibility()
        });
    }

    componentWillUnmount(){
        UserPageDisplayStore.removeListener('change', this.handleDisplayChange);
    }

    constructor(props){
        super(props);

        this.state = {
            componentsVisibility : {}
        };

        this.handleDisplayChange = this.handleDisplayChange.bind(this);
    }

    handleDisplayChange(){
        this.setState({
            componentsVisibility : UserPageDisplayStore.getComponentsVisibility()
        });
    }



    render(){
        console.log("visibilit = ", this.state.componentsVisibility);
        return (
            <div>
                {this.state.componentsVisibility['gamesList'] ? <GamesList /> : null}
            </div>
        );
    }
}

export default GamesMainComponent;
