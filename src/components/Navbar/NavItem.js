import React, {Component} from 'react';

import '../../css/stylesheet.css';

class NavItem extends Component {

    constructor(props){
        super(props);
    }

    render(){


        console.log("props in navItem = ", this.props);
        return (
            <div className="nav-item">
                <b onClick={e => this.props.onChangeComponentVisibility(e, this.props.itemName)}>
                    {this.props.name}
                </b>
            </div>

        );
    }
}

export default NavItem;

