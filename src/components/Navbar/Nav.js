import React, {Component} from 'react';

import '../../css/stylesheet.css';

class NavbarComponent extends Component {

    constructor(props){
        super(props);
    }

    render(){

        let style = {clear : 'both'};

        return (
            <nav className="nav">
                <div className="nav-wrapper">
                    {this.props.children}
                    <span style={style}></span>
                </div>

            </nav>

        );
    }
}

export default NavbarComponent;

