import React, {Component} from 'react';

import '../../css/stylesheet.css';

class NavRouteItem extends Component {

    constructor(props){
        super(props);
    }

    render(){

        return (
            <div className="nav-route-item">
                <b><a href={this.props.href}>
                    {this.props.name}
                </a></b>
            </div>

        );
    }
}

export default NavRouteItem;

