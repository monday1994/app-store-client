import React, {Component} from 'react';

import '../css/stylesheet.css';

class Header extends Component {

    constructor(props){
        super(props);
    }

    render(){

        return (
            <header className="header">
                <p>
                    <b>App Store</b>
                </p>
            </header>

        );
    }
}

export default Header;

