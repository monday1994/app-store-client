let httpPort = 3001;
//react port is 40402
let sioPort = 3001;

let serverUrl = 'http://localhost:'+httpPort;

module.exports = {
    serverUrl : serverUrl,
    sioPort : sioPort,
    httpPort : httpPort,
    //cookies expire in 60 mins
    cookiesExpirationTime : new Date((Date.now() + (1000 * 60 * 60)))
};