import VC from './validationConstants';
const regex = require('./regex');

export function validateUserEmailAndPassword(user, errors){
    if(!regex.emailRegex.test(user.email)){
        errors.email.addError('email is invalid');
    }

    if(!regex.allCharactersUsedInWriting.test(user.password)){
        errors.password.addError('password is invalid');
    }

    return errors;
}
