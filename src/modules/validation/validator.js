
import VC from './validationConstants';
const regex = require('./regex');

export function validateEmail(email){
    if(email.length > VC.EMAIL_MIN_LENGTH && email.length < VC.EMAIL_MAX_LENGTH){
        return regex.emailRegex.test(email);
    } else {
        return false;
    }
}

export function validatePassword(password){
    if(password.length > VC.PASSWORD_MIN_LENGHT && password.length < VC.PASSWORD_MAX_LENGTH){
        return regex.allCharactersUsedInWriting.test(password);
    } else {
        return false;
    }
}

export function validateName(name){
    if(name.length > VC.NAME_MIN_LENGTH && name.length < VC.NAME_MAX_LENGTH){
        return regex.allCharactersUsedInWriting.test(name);
    } else {
        return false;
    }
}