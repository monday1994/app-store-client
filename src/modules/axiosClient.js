import cookiesWrapper from './cookiesWrapper';
const axios = require('axios');
const config = require('../config');

const axiosPostInstance = axios.create({
    baseURL: config.serverUrl,
    headers: {
        'Content-Type': 'application/json'
    }
});

const axiosGetInstance = axios.create({
    baseURL: config.serverUrl,
    headers : {
        'Accept' : 'application/json'
    }
});


let axiosConfig = {
    axiosPost: axiosPostInstance,
    axiosGet: axiosGetInstance
};

export default axiosConfig;

