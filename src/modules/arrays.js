const _ = require('lodash');

export function removeObjectByKey(arr, key){
    let foundObject = _.find(arr, key);
    return _.pull(arr, foundObject);
}

