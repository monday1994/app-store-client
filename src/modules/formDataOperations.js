import util from 'util';

const checkNumOfElems = function(objToBeCheck, counter){
    Object.keys(objToBeCheck).forEach((elem) => {
        if (objToBeCheck[elem] !== null && typeof objToBeCheck[elem] === 'object') {
            checkNumOfElems(objToBeCheck[elem], counter);
            return;
        }

        if(elem.length > 1 && isNaN(elem)){
            counter.num++;
        }
    });

    return counter;
};

export function findFilesToRemove(newConfigObject, filesFromServer){

    return new Promise((resolve,reject) => {
        Object.keys(newConfigObject).forEach((elem) => {
            if (newConfigObject[elem] !== null && typeof newConfigObject[elem] === 'object') {
                findFilesToRemove(newConfigObject[elem], filesFromServer);
                return;
            }

            if(elem.length > 1 && isNaN(elem)){

                if(elem.startsWith('_url_')){
                    filesFromServer.forEach((currentFileLink, i)=> {
                        if(currentFileLink == newConfigObject[elem]){
                            let idx = filesFromServer.indexOf(currentFileLink);
                            filesFromServer.splice(idx, 1);
                        }
                    });
                }
            }
        });
        resolve(filesFromServer);
    });
};


