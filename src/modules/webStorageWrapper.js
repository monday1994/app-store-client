import WebStorage from 'react-webstorage';
import util from 'util';

class WebStorageWrapper {

    constructor(props){

        this.get = this.get.bind(this);
        this.add = this.add.bind(this);
        this.remove = this.remove.bind(this);
        this.clear = this.clear.bind(this);
    }
    webStorage = new WebStorage(window.localStorage || window.sessionStorage);

    get(key){
        console.log("key = ", key);
        try{
            return this.webStorage.getItem(key);
        } catch(err){
            console.log("err in webStorage.get ", util.inspect(err));
        }

    }

    add(key, value){
        console.log("key = ", key);
        console.log("value = ", value);
        try{
            this.webStorage.setItem(key, value);
        } catch(err){
            console.log("err in webStorage.add ", util.inspect(err));
        }
    }

    remove(key){
        try{
            this.webStorage.removeItem(key);
        } catch(err){
        console.log("err in webStorage.remove ", util.inspect(err));
    }

    }

    clear(){
        try{
            this.webStorage.clear();
        } catch(err){
            console.log("err in webStorage.clear ", util.inspect(err));
        }
    }
}

const webStorageWrapper = new WebStorageWrapper();

export default webStorageWrapper;
