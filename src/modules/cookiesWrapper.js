import Cookies from 'universal-cookie';

import util from 'util';

const config = require('../config');

class CookiesWrapper {
    constructor(props){
        // super(props);

        this.get = this.get.bind(this);
        this.getMultiple = this.getMultiple.bind(this);
        this.add = this.add.bind(this);
        this.addMultiplePairs = this.addMultiplePairs.bind(this);
        this.remove = this.remove.bind(this);
        this.removeMultiple = this.removeMultiple.bind(this);
    }

    cookies = new Cookies();

    get(key){
        try{
            return this.cookies.get(key);
        } catch(err){
            console.log("err in cookies.get ", util.inspect(err));
        }

    }

    getMultiple(keys){
        let obj = {};

        keys.map((key) => {
            obj[key] = this.get(key);
        });

        return obj;
    }

    add(key, value){
        try{
            this.cookies.set(key, value, {path : '/', expires : config.cookiesExpirationTime});
        } catch(err){
            console.log("err in cookies.add ", util.inspect(err));
        }
    }

    addMultiplePairs(keyValuePairs){

        for(let elem in keyValuePairs){
            this.add(elem, keyValuePairs[elem]);
        }

    }

    remove(key){
        try{
            this.cookies.remove(key);
        } catch(err){
            console.log("err in cookies.remove ", util.inspect(err));
        }
    }

    removeMultiple(keys){
        keys.map((currentKey) => {
            this.remove(currentKey);
        });
    }
}

const cookiesWrapper = new CookiesWrapper();

export default cookiesWrapper;
