import util from 'util'

import { default as axios } from '../axiosClient';

const axiosPost = axios.axiosPost;

export function loginCall(loginData){

    return new Promise((resolve, reject)=>{
        axiosPost.post('/auth/login', loginData).then((response) => {

            let data = {
                id : response.data.id,
                role : response.data.role
            };
            resolve(data);
        }).catch((error) => {
            console.error('err = ' + util.inspect(error));
            reject(error);
        });
    });
}

export function registrationCall(user){

    return new Promise((resolve, reject)=>{
        axiosPost.post('/auth/userRegistration', user).then((response) => {

            resolve(response.data.newUser);
        }).catch((error) => {
            console.error('err = ' + util.inspect(error));
            reject(error);
        });
    });
}