import util from 'util'
//flux import

import { default as axios } from '../../axiosClient';

export function createGameCall(data){
    const axiosPost = axios.axiosPost;
   
    return new Promise((resolve, reject) => {

       let body = {
            name : data.name,
            description: data.description,
            version : data.version,
            thumbnail: data.thumbnail,
            game : data.game
       };

       console.log("body in games calls = ", body);

       axiosPost.post('/games/createGame', body).then(response => {
            resolve(response.data.createdGame);
       }).catch(err => {
            reject(err);
       });

      /* const configuration = {
            onUploadProgress: (progressEvent) => {
                let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                FilesActions.updateProgressValue(percentCompleted);
            }
        };
           */
        /*axiosJWTPost.post('/games/createGame', body, configuration).then(response => {
            resolve(response.data.createdGame);
        }).catch(err=>{
            console.log("error in create game = "+util.inspect(err));
            reject(err);
        });*/
    });
}

export function fetchAllGames(){
    const axiosGet = axios.axiosGet;
    return new Promise((resolve, reject)=>{
        axiosGet('/games/fetchAllGames').then(response => {
            resolve(response.data.games);
        }).catch(err => {
            console.log("err in fetchAllGames");
            reject(err);
        });
    });
}

export function editGameCall(data){
    const axiosPost = axios.axiosPost;

    return new Promise((resolve, reject) => {

        let body = {
            id : data.id,
            name : data.name,
            description: data.description,
            version : data.version,
            thumbnail: data.thumbnail,
            game : data.game
        };

        console.log("body in games edit call = ", body);

        axiosPost.post('/games/editGame', body).then(response => {
            resolve(response.data.updatedGame);
        }).catch(err => {
            reject(err);
        });
    });
}

export function removeGameCall(gameId){
    const axiosPost = axios.axiosPost;

    return new Promise((resolve, reject) => {

        let body = {
            gameId : gameId,
        };

        axiosPost.post('/games/removeGame', body).then(response => {
            resolve(true);
        }).catch(err => {
            reject(err);
        });
    });
}




