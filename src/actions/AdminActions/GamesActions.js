import dispatcher from '../../dipstacher';

export function createGame(data){
    dispatcher.dispatch({
        type : "CREATE_GAME",
        data : data
    });
}

export function editGame(data){
    dispatcher.dispatch({
        type : "EDIT_GAME",
        data : data
    });
}

export function removeGame(data){
    dispatcher.dispatch({
        type : "REMOVE_GAME",
        data : data
    });
}

export function setGameToBeEdit(data){
    dispatcher.dispatch({
        type : "SET_GAME_TO_BE_EDIT",
        data : data
    });
}

export function unsetGameToBeEdit(){
    dispatcher.dispatch({
        type : "UNSET_GAME_TO_BE_EDIT"
    });
}





