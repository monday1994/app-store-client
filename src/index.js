import React from 'react';
import ReactDOM from 'react-dom';

import registerServiceWorker from './registerServiceWorker';
import {Switch, Route, BrowserRouter} from 'react-router-dom';



//pages import
import Home from './pages/Home';
import User from './pages/User';
import Admin from './pages/Admin';
import Login from './pages/Login';
import Registration from './pages/Registration';


ReactDOM.render(
    <BrowserRouter >
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/registration" component={Registration}/>
            <Route path="/user" component={User}/>
            <Route path="/admin" component={Admin}/>
        </Switch>
    </BrowserRouter>,
    document.getElementById('root')
);

registerServiceWorker();
