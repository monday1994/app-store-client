import {EventEmitter} from 'events';
import dispatcher from '../../dipstacher';
import util from 'util';

import Promise from 'bluebird';

class AdminPageDisplayStore extends EventEmitter {

    constructor(props) {
        super(props);

        this.componentsVisibility = {
            gamesMainComponent : true,
            gamesList : true,
            createGame : false,
            editGame : false
        }
    }

    getComponentVisibility(name){
        return this.componentsVisibility[name];
    }

    getComponentsVisibility(){
        return this.componentsVisibility
    }

    setComponentsVisibility(componentsNames){
        for(let elem in this.componentsVisibility){
            this.componentsVisibility[elem] = false;
        }
        componentsNames.forEach((currentName => {
            this.componentsVisibility[currentName] = true;
        }));
        this.emit('change');
    }

    handleActions(action){
        switch(action.type){
            case "SET_COMPONENTS_VISIBILITY" : {
                this.setComponentsVisibility(action.data);
                break;
            }
        }
    }
}

const adminPageDisplayStore = new AdminPageDisplayStore();

dispatcher.register(adminPageDisplayStore.handleActions.bind(adminPageDisplayStore));

export default adminPageDisplayStore;