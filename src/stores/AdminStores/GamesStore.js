import {EventEmitter} from 'events';
import dispatcher from '../../dipstacher';
import util from 'util';

import * as gamesCalls from '../../modules/api/userCalls/gamesCalls';

import Promise from 'bluebird';
import * as arrays from '../../modules/arrays';

class GamesStore extends EventEmitter {

    constructor(props) {
        super(props);

        this.games = [];
        this.gameToBeEdit = undefined;
        this.progressValue = 0;
    }

    initializeStore(){
        gamesCalls.fetchAllGames().then(games =>{
            this.games = games;
            this.emit('change');
        }).catch(err => {
            console.log("err in gamesStore.initializeStore, err = ", err);
        });
    }

    getGames(){
        return this.games;
    }

    getGameToBeEdit(){
        return this.gameToBeEdit;
    }

    getProgressValue(){
        return this.progressValue;
    }

    createGame(data){
        console.log("data ", data);
        gamesCalls.createGameCall(data).then((createdGame => {
            this.games.push(createdGame);
            this.emit('change');
        })).catch(err => {
            console.log("err in GamesStore, err = ", err);
        });
    }

    editGame(data){
        gamesCalls.editGameCall(data).then(updatedGame => {
            arrays.removeObjectByKey(this.games, { id : data.id });
            this.games.push(updatedGame);
            this.gameToBeEdit = undefined;
            this.emit('change');
        }).catch(err => {
            console.log("err in gamesStore.editGame, err = ", err);
        });
    }

    removeGame(data){
        gamesCalls.removeGameCall(data.id).then(result => {
            arrays.removeObjectByKey(this.games, {id : data.id});
            this.emit('change');
        }).catch(err => {
            console.log("err in gamesStore.removeGame, err = ", err);
        })
    }

    setGameToBeEdit(data){
        this.gameToBeEdit = data;
    }

    unsetGameToBeEdit(data){
        this.gameToBeEdit = undefined;
    }

    handleActions(action){
        switch(action.type){
            case "CREATE_GAME" : {
                this.createGame(action.data);
                break;
            }
            case "EDIT_GAME" : {
                this.editGame(action.data);
                break;
            }
            case "REMOVE_GAME" : {
                this.removeGame(action.data);
                break;
            }
            case "SET_GAME_TO_BE_EDIT" : {
                this.setGameToBeEdit(action.data);
                break;
            }
            case "UNSET_GAME_TO_BE_EDIT" : {
                this.unsetGameToBeEdit();
                break;
            }
        }
    }
}

const gamesStore = new GamesStore();

dispatcher.register(gamesStore.handleActions.bind(gamesStore));

export default gamesStore;