import {EventEmitter} from 'events';
import dispatcher from '../../dipstacher';
import util from 'util';

import Promise from 'bluebird';

class UserPageDisplayStore extends EventEmitter {

    constructor(props) {
        super(props);

        this.componentsVisibility = {
            gamesMainComponent : true,
            gamesList : true
        }
    }

    getComponentVisibility(name){
        return this.componentsVisibility[name];
    }

    getComponentsVisibility(){
        return this.componentsVisibility
    }

    setComponentsVisibility(componentsNames){
        for(let elem in this.componentsVisibility){
            this.componentsVisibility[elem] = false;
        }
        componentsNames.forEach((currentName => {
            this.componentsVisibility[currentName] = true;
        }));
        this.emit('change');
    }

    handleActions(action){
        switch(action.type){
            case "SET_COMPONENTS_VISIBILITY" : {
                this.setComponentsVisibility(action.data);
                break;
            }
        }
    }
}

const userPageDisplayStore = new UserPageDisplayStore();

dispatcher.register(userPageDisplayStore.handleActions.bind(userPageDisplayStore));

export default userPageDisplayStore;